# Read and display COVID-19 data from https://api.covid19api.com - J.Cuenca 2020
#
# This program is free software and is distributed under the terms of the GNU General Public License, version 3 or any later version. A copy of the license can be found here: https://www.gnu.org/licenses/gpl-3.0.en.html

import requests
import ast
from datetime import datetime
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.dates as md
from cycler import cycler
from population import *

r={}
cases={}
active={}
deaths={}
recovered={}
dates={}
time={}
newcases={}
daydeaths={}
daydeathsweekavg={}

countries=("china", "united-states", "italy", "spain", "france", "united-kingdom", "germany", "belgium", "colombia")
# countries=("china", "united-states", "italy", "spain", "france", "united-kingdom", "germany", "belgium", "sweden")
# countries=("china", "united-states", "italy", "spain", "france", "united-kingdom", "belgium", "colombia", "brazil")

for country in countries:
    r = requests.get('https://api.covid19api.com/total/country/'+country+'/status/confirmed')
    t = ast.literal_eval(r.text)
    cases[country] = np.array([t[i]["Cases"] for i in range(len(t))])
    dates[country] = np.array([t[i]["Date"] for i in range(len(t))])
    
    r = requests.get('https://api.covid19api.com/total/country/'+country+'/status/deaths')
    t = ast.literal_eval(r.text)
    deaths[country] = np.array([t[i]["Cases"] for i in range(len(t))])
    
    r = requests.get('https://api.covid19api.com/total/country/'+country+'/status/recovered')
    t = ast.literal_eval(r.text)
    recovered[country] = np.array([t[i]["Cases"] for i in range(len(t))])
    
    time[country] = np.array([int(datetime.strptime(dates[country][i],'%Y-%m-%dT%H:%M:%SZ').timestamp()) for i in range(len(t))])
    dates[country] = np.array([datetime.fromtimestamp(time[country][i]).strftime('%Y-%m-%d') for i in range(len(t))])
    active[country] = cases[country] - recovered[country] - deaths[country]
    newcases[country] = np.append(0,np.diff(cases[country]))
    daydeaths[country] = np.append(0,np.diff(deaths[country]))
    daydeathsweekavg[country] = np.convolve(daydeaths[country],np.ones((7,))/7,mode="valid")



## PLOTS ================================================================================



pl.rcdefaults()
pl.rc('lines', linewidth=2)
pl.rc('axes', prop_cycle=(cycler('color', ['k','b','g','r','m','c']) + cycler('linestyle', ['--','-','-','-','-','-'])))


fig = pl.figure()
tmp=4
m=3
for n in range(len(countries)):
    country = countries[n]
    p = population[countries[n]]/1e6
    ax = pl.subplot(np.ceil(len(countries)/m), m, n+1)
    ax.plot(time[country],cases[country]/p,label="total cases")
    ax.plot(time[country],active[country]/p,label="active")
    ax.plot(time[country],recovered[country]/p,label="recovered")
    ax.plot(time[country],deaths[country]/p,label="deaths")
    ax.plot(time[country],daydeaths[country]/p,label="daily deaths",linewidth=1)
    ax.plot(time[country][len(time[country])-len(daydeathsweekavg[country]):],daydeathsweekavg[country]/p,label="daily deaths week avg",linewidth=1)
    ax.set_title(country)
    ax.set_xticklabels([])
    if n>=len(countries)-m:
        ax.set_xticks(time[countries[0]][::tmp])
        ax.set_xticklabels(dates[countries[0]][::tmp], rotation="vertical" )
fig.suptitle("Cases per million people",x=.2)
handles, labels = ax.get_legend_handles_labels()
fig.legend(handles, labels, loc='upper center',prop={'size': 8})
fig.subplots_adjust(bottom=0.2,top=.85)
# fig.savefig("a")

fig = pl.figure()
tmp=4
m=3
for n in range(len(countries)):
    country = countries[n]
    p = population[countries[n]]/1e6
    ax = pl.subplot(np.ceil(len(countries)/m), m, n+1)
    ax.semilogy(time[country],cases[country]/p,label="total cases")
    ax.plot(time[country],active[country]/p,label="active")
    ax.plot(time[country],recovered[country]/p,label="recovered")
    ax.plot(time[country],deaths[country]/p,label="deaths")
    ax.plot(time[country],daydeaths[country]/p,label="daily deaths",linewidth=1)
    ax.plot(time[country][len(time[country])-len(daydeathsweekavg[country]):],daydeathsweekavg[country]/p,label="daily deaths week avg",linewidth=1)
    ax.set_title(country)
    ax.set_xticklabels([])
    if n>=len(countries)-m:
        ax.set_xticks(time[countries[0]][::tmp])
        ax.set_xticklabels(dates[countries[0]][::tmp], rotation="vertical" )
fig.suptitle("Cases per million people (log scale)",x=.2)
handles, labels = ax.get_legend_handles_labels()
fig.legend(handles, labels, loc='upper center',prop={'size': 8})
fig.subplots_adjust(bottom=0.2,top=.85)
# fig.savefig("b")



pl.rcdefaults()
pl.rc('lines', linewidth=2)

fig, ax = pl.subplots(nrows=2, ncols=1, sharex=True)
tmp=2
for n in range(len(countries)):
    country = countries[n]
    ax[0].plot(time[country],active[country],label=country)
ax[0].set_ylabel('active cases')
ax[0].set_xlabel('time')
ax[0].legend()
ax[0].set_ylim(bottom=1)

for n in range(len(countries)):
    country = countries[n]
    ax[1].plot(time[country],active[country]/population[country]*1e6,label=country)
ax[1].set_ylabel('active cases per million people')
ax[1].set_xlabel('time')
ax[1].legend()
ax[1].set_xticks(time[countries[0]][::tmp])
ax[1].set_xticklabels(dates[countries[0]][::tmp], rotation="vertical" )
fig.suptitle("Active cases")
fig.subplots_adjust(bottom=0.2)
# fig.savefig("c")



pl.rcdefaults()
pl.rc('lines', linewidth=2)

fig, ax = pl.subplots(nrows=2, ncols=1, sharex=True)
tmp=2
for n in range(len(countries)):
    country = countries[n]
    ax[0].plot(time[country],deaths[country],label=country)
ax[0].set_ylabel('Deaths')
ax[0].set_xlabel('time')
ax[0].legend()
ax[0].set_ylim(bottom=1)

for n in range(len(countries)):
    country = countries[n]
    ax[1].plot(time[country],deaths[country]/population[country]*1e6,label=country)
ax[1].set_ylabel('Deaths per million people')
ax[1].set_xlabel('time')
ax[1].legend()
ax[1].set_xticks(time[countries[0]][::tmp])
ax[1].set_xticklabels(dates[countries[0]][::tmp], rotation="vertical" )
fig.suptitle("Deaths")
fig.subplots_adjust(bottom=0.2)
# fig.savefig("c")




fig, ax = pl.subplots(nrows=2, ncols=1, sharex=False) # True
tmp=2
for n in range(len(countries)):
    country = countries[n]
    p = population[countries[n]]/1e6
    ax[0].loglog(deaths[country][len(deaths[country])-len(daydeathsweekavg[country]):]/p,daydeathsweekavg[country]/p,label=country)
    ax[1].loglog(deaths[country]/p,active[country]/p,label=country)
ax[0].legend()
ax[0].set_xlabel('deaths per million people')
ax[0].set_ylabel('daily deaths per million people (sliding week average)')
ax[0].grid(1,which="both")
ax[1].grid(1,which="both")
ax[1].set_xlabel('deaths per million people')
ax[1].set_ylabel('active cases per million people')
ax[1].legend()
fig.suptitle("Deaths per million people")
# fig.savefig("d")



pl.show()
